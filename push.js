const push = require('web-push');

const vapidKeys = {
	publicKey: 'BPAK15_6iJfmvQmgFE5R0KgIR2T4E-cCEitboU48cUTllCidv_XUWRPm7-O-U7j_9Sj9i6mSDewO17fBR1Aad64',
	privateKey: 'cOc1xoLoaEuxYEBzHylU6PhxQ3acy2lEIZAlGYyrESY',
};

push.setVapidDetails('mailto:isingwithaz@gmail.com', vapidKeys.publicKey, vapidKeys.privateKey);

const sub = {
	endpoint:
		'https://fcm.googleapis.com/fcm/send/dMRYbKl44YU:APA91bFZlvTNaLRVJfrlIcMDu-rS7qhTNMZePyCbxfEXg3x32NzMieYhSrKkB4PQhBshiKYIh8yYW-6jwm8jpJTyOpiM-jt5VA1mT_AJRgu8QLGNMAPM6LQqFfXWWKRYq4id6-q8Fsam',
	expirationTime: null,
	keys: {
		p256dh: 'BC5ByBksYRMQDkjJo4-EKW_WpSzeEMS77uT2ngsCUK1r6JUll3Er59qXFv6ldJDqTqkNK7g-cXtaJkwJaljMN-8',
		auth: 'AtsDFa3awymGtv9dhE4jWA',
	},
};

push.sendNotification(
	sub,
	JSON.stringify({
		url: '/test',
		message: 'Pavel',
		actions: [
			{ action: 'open', title: 'Open' },
			{ action: 'close', title: 'Close' },
		],
	})
);
