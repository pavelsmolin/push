self.addEventListener('push', function (e) {
	const {
		actions = [
			{ action: 'open', title: 'Open' },
			{ action: 'close', title: 'Close' },
		],
		message,
		url,
	} = e.data
		? e.data.json()
		: {
				actions: [
					{ action: 'open', title: 'Open' },
					{ action: 'close', title: 'Close' },
				],
				message: 'Notification message',
				url: '/',
		  };
	const options = {
		body: message,
		icon: 'images/checkmark.png',
		vibrate: [100, 50, 100],
		data: {
			url,
		},
		actions,
	};

	e.waitUntil(self.registration.showNotification('HIRO Desktop', options));
});

self.addEventListener('notificationclick', function (event) {
	const { url = '' } = event.notification.data;

	event.notification.close();

	const preparedUrl = `${event.target.location.origin}${url}`;

	event.waitUntil(
		clients
			.matchAll({
				type: 'window',
			})
			.then(function (clientList) {
				for (let i = 0; i < clientList.length; i++) {
					const client = clientList[i];

					if (client.url === preparedUrl && typeof client.focus === 'function') {
						return client.focus();
					}
				}

				if (typeof clients.openWindow === 'function') {
					return clients.openWindow(preparedUrl);
				}
			})
	);
});
